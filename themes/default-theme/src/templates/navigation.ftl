<nav class="${nav_css_class} navbar-expand-md" id="navigation" role="navigation">
	<div class="collapse navbar-collapse" id="navigationMenu">
		<ul role="menubar" class="navbar-nav">
			<#list nav_items as nav_item>
				<#assign
					nav_item_attr_has_popup = ""
					nav_item_css_class = ""
					nav_item_layout = nav_item.getLayout()
				/>

				<#if nav_item.isSelected()>
					<#assign
						nav_item_attr_has_popup = "aria-haspopup='true'"
						nav_item_css_class = "selected"
					/>
				</#if>

				<li class="${nav_item_css_class}" id="layout_${nav_item.getLayoutId()}" role="presentation">
          <#if nav_item.hasChildren()>
						<div class="dropdown">
							<button type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<@liferay_theme["layout-icon"] layout=nav_item_layout /> ${nav_item.getName()}
							</button>

							<div class="dropdown-menu">
								<#list nav_item.getChildren() as nav_child>
									<#assign
										nav_child_css_class = ""
									/>

									<#if nav_item.isSelected()>
										<#assign
											nav_child_css_class = "selected"
										/>
									</#if>

                  <a href="${nav_child.getURL()}" class="dropdown-item" role="menuitem">
										${nav_child.getName()}
									</a>
              	</#list>
							</div>
						</div>

            <#else>
              <a href="${nav_item.getURL()}" role="menuitem" ${nav_item_attr_has_popup} ${nav_item.getTarget()}>
								<@liferay_theme["layout-icon"] layout=nav_item_layout /> ${nav_item.getName()}
							</a>
          </#if>
        </li>
			</#list>
		</ul>
	</div>
</nav>
