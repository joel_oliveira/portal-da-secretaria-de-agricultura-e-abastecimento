<div id="accessibility_bar">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="wrapper">
					<div class="websites">
						<a href="http://www.saopaulo.sp.gov.br/" target="_blank" rel="noopener noreferrer" class="item">
							saopaulo.sp.gov.br
						</a>

						<a href="http://www.cidadao.sp.gov.br/" target="_blank" rel="noopener noreferrer" class="item">
							Cidadão SP
						</a> 	
					</div>

					<div class="utilities">
						<div class="social-networks">
							<a href="https://www.facebook.com/agriculturasp" class="item" target="_blank" rel="noopener noreferrer">
								<img src="${images_folder}/icon-facebook.png" alt="Facebook" class="img-fluid">
							</a>
	
							<a href="https://twitter.com/agriculturasp" class="item" target="_blank" rel="noopener noreferrer">
								<img src="${images_folder}/icon-twitter.png" alt="Twitter" class="img-fluid">
							</a>
	
							<a href="https://www.instagram.com/agriculturasp/" class="item" target="_blank" rel="noopener noreferrer">
								<img src="${images_folder}/icon-instagram.png" alt="Instagram" class="img-fluid">
							</a>
	
							<a href="https://www.flickr.com/photos/agriculturasp/albums" class="item" target="_blank" rel="noopener noreferrer">
								<img src="${images_folder}/icon-flickr.png" alt="Flickr" class="img-fluid">
							</a>
	
							<a href="https://www.youtube.com/agriculturasp" class="item" target="_blank" rel="noopener noreferrer">
								<img src="${images_folder}/icon-youtube.png" alt="YouTube" class="img-fluid">
							</a>
						</div>

						<span>/governosp</span>
						
						<div class="accessibility-tools">
							<button type="button" class="item" id="defaultFontSize">
								<img src="${images_folder}/default-font-size.png" alt="Texto Normal" class="img-fluid">
							</button>

							<button type="button" class="item" id="decreaseFontSize">
								<img src="${images_folder}/decrease-font.png" alt="Diminuir Texto" class="img-fluid">
							</button>

							<button type="button" class="item" id="increaseFontSize">
								<img src="${images_folder}/increase-font.png" alt="Aumentar Texto" class="img-fluid">
							</button>

							<button type="button" class="item" id="defaultContrast">
								<img src="${images_folder}/default-contrast.png" alt="Contraste Inicial" class="img-fluid">
							</button>

							<button type="button" class="item" id="highContrast">
								<img src="${images_folder}/contrast.png" alt="Alto Contraste" class="img-fluid">
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
