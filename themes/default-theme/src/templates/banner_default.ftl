<header id="banner" role="banner">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="wrapper">
					<div class="portal-content col-10 col-md-5" style="background-image: url(${images_folder}/bandeira-separa.jpg);">
						<h3>Agricultura e Abastecimento</h3>
					</div>

					<div class="department col-md-7 d-md-flex d-none">
						<img src="${images_folder}/logotipo-gov-sp.jpg" alt="Governo do Estado de São Paulo">
					</div>

					<div class="menu-toggle col-2 d-md-none">
						<button type="button" class="navbar-toggler collapsed" data-toggle="collapse" aria-expanded="false" data-target="#navigationMenu">
							<span class="navbar-toggler-icon"></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
