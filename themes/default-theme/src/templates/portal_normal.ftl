<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${html_title}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class}">
	<@liferay_ui["quick-access"] contentId="#main-content" />

	<@liferay_util["include"] page=body_top_include />

	<@liferay.control_menu />

	<div id="wrapper">
		<#include "${full_templates_path}/accessibility_bar.ftl" />

		<#include "${full_templates_path}/banner_default.ftl" />

		<div id="navigation_menu">
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<div class="wrapper">
						    <@liferay.user_personal_bar />

							<#if has_navigation && is_setup_complete>
								<#include "${full_templates_path}/navigation.ftl" />
							</#if>
						</div>
					</div>
				</div>
			</div>
		</div>

		<section id="content">
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<div class="wrapper">
							<h2 class="hide-accessible" role="heading" aria-level="1">${the_title}</h2>

							<#if selectable>
								<@liferay_util["include"] page=content_include />

								<#else>
									${portletDisplay.recycle()}

									${portletDisplay.setTitle(the_title)}

									<@liferay_theme["wrap-portlet"] page="portlet.ftl">
										<@liferay_util["include"] page=content_include />
									</@>
							</#if>
						</div>
					</div>
				</div>
			</div>
		</section>

		<footer id="footer" role="contentinfo">
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<div class="wrapper">
							<ul class="websites">
								<li class="item">
									<a href="https://www.ouvidoria.sp.gov.br/Portal/ComoPossoAjudar.aspx?cod_prestador=399" target="_blank" rel="noopener noreferrer">
										Ouvidoria
									</a>
								</li>

								<li class="item">
									<a href="http://www.transparencia.sp.gov.br/" target="_blank" rel="noopener noreferrer">Transparência</a>
								</li>

								<li class="item">
									<a href="http://www.sic.sp.gov.br/" target="_blank" rel="noopener noreferrer">SIC</a>
								</li>
							</ul>

							<figure class="footer-logotipo">
								<img src="${images_folder}/logotipo-gov-sp.jpg" alt="Governo do Estado de São Paulo">
							</figure>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<@liferay_util["include"] page=body_bottom_include />

	<@liferay_util["include"] page=bottom_include />

	<script src="${javascript_folder}/jquery.slim.min.js" type="text/javascript"></script>

	<script src="${javascript_folder}/popper.min.js" type="text/javascript"></script>
	<script src="${javascript_folder}/bootstrap.min.js" type="text/javascript"></script>
</body>

</html>
