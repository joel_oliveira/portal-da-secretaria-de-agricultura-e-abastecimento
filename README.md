## Liferay DXP com IntelliJ

Este documento descreve como configurar o seu ambiente de desenvolvimento para construir e executar projetos Liferay. Também explica a mecânica básica para uso do `Git`, `NVM`, `NodeJS` e `Yarn`.

- [Preparação de ambiente](#preparação-de-ambiente)
    - [Pré-requisitos](#pre-requisitos)
    - [Instalação e configuração](#instalação-e-configuração)
        - [Java](#java)
        - [NVM](#nvm)
            - [Node](#node)
                - [Yarn](#yarn)
        - [IntelliJ](#intellij)
            - [Liferay Plugin & Liferay Workspace](#liferay-plugin-&-liferay-workspace)
        - [JPM](#jpm)
            - [Blade](#blade)
        - [PostgreSQL](#postgresql)
- [Configurando o Portal Liferay](#configurando-o-portal-liferay)

## Preparação de ambiente

Segue o passo a passo necessário para a preparação do ambiente de desenvolvimento, abordando os procedimentos necessários para instalação e configuração das ferramentas adotadas pelo time técnico.

### Pré-requisitos

Antes de iniciar a construção de novos portais, você deve instalar e configurar as seguintes ferramentas na sua máquina de desenvolvimento:

- [Git](https://git-scm.com/) (versão `>= 2.25.1`) instalado, permitindo o controle de versões do código-fonte produzido.

Para assegurar que as ferramentas citadas acima estejam devidamente instaladas na sua máquina, execute os comandos abaixo:

```shell
# Printa a versão do Git instalado na máquina
git --version
```

Além das ferramentas já citadas, é recomendado pelo menos 8 GB de RAM para um bom desempenho das ferramentas.

### Instalação e configuração

Neste tópico será demonstrado os procedimentos para a instalação e configuração de cada ferramenta utilizada no desenvolvimento das demandas, garantindo a padronização entre os colaboradores dos projetos.

#### Java

O Liferay DXP requer um Java (JDK) 8 ou 11 neste tutorial iremos utilizar o [Java 8](https://www.oracle.com/br/java/technologies/javase/javase8-archive-downloads.html).

Após a instalação do [Java 8](https://www.oracle.com/br/java/technologies/javase/javase8-archive-downloads.html) será necessário configurar duas Variáveis de Ambiente, uma Variável de Usuário e outra Variável do Sistema.

Para adicionar a *Variável de Usuário*, basta criar uma variável com o nome `JAVA_HOME` tendo como valor o diretório para a sua JRE, caso tenha instalado o Java no diretório padrão o valor será `C:\Program Files\Java\jdk1.8.0_202\jre`.

![Variável de Usuário](docs/variavel-de-usuario.PNG)

Para adicionar a *Variável do Sistema*, basta editar a variável _Path_ e adicionar um novo parâmetro com o valor `%JAVA_HOME%\bin`.

![Variável do Sistema](docs/variavel-do-sistema.PNG)

Para assegurar que o Java foi instalado corretamente, execute o seguitne comando:

```shell
# Printa a versão do Java instalado na máquina.
java -version
```

#### NVM

>**NOTA:** Caso tenha o Node JS na sua máquina, identifique qual é a sua versão e o desinstale. Após a conclusão deste e do próximo passo você poderá baixa-la através do NVM

O [NVM](https://github.com/coreybutler/nvm-windows/releases) é uma ferramenta de linha de comando que possibilita instalar e gerenciar diversas versões do NodeJS.

Para instalar o [NVM](https://github.com/coreybutler/nvm-windows/releases) baixe o arquivo `zip` do nvm-setup.

Para assegurar que o NVM foi instalado corretamente, execute o seguinte comando:

```shell
# Printa a versão do NVM instalado na máquina.
nvm version
```

> O NVM é executado em um Shell de administrador. Você precisará iniciar o PowerShell ou Prompt de Comando como Administrador para usar o NVM.

#### Node

Nos projetos Liferay iremos utilizar a versão 10.18.1 do Node.

A instalação do Node será feita através do seu Gerenciador de Versões (NVM) possibilitando que tenha mais de uma versão do NodeJS instalado na sua máquina.

Para instalar uma versão do NodeJS você pode especificar uma versão (no nosso caso a será a versão 10.18.1), utilizar a palavra-chave "latest" para a versão atual mais recente ou a palavra-chave "lts" para a verão LTS mais recente.

```shell
# Baixa e Instala o NodeJS na versão específicada.
nvm install 10.18.1

# Alterna para usar a versão especificada. Opcionalmente, use "latest", "lts" ou "newest". newest é a versão instalada mais recente.
nvm use 10.18.1

# Para assegurar que o Node foi instalado corretamente, execute o seguinte comando:

node --version
```

Também poderá utilizar o comando `nvm list` para listar todas as instalações do NodeJS.

#### Yarn

Em nossos projetos iremos utilizar a versão 1.22.15 do Yarn. Para isso basta instala-lo a partir do Node Package Manager (NPM)

```shell
npm i -g yarn@1.22.15

# Para assegurar que o Yarn foi instalado corretamente, execute o seguinte comando:
yarn --version
```

#### IntelliJ

IDE, ou ambiente de desenvolvimento integrado, o [IntelliJ](https://www.jetbrains.com/pt-br/idea/) é um software que combina ferramentas comuns de desenvolvimento Java em uma única interface gráfica do usuário (GUI), facilitando o desenvolvimento de aplicações.

Durante a instalação do IntelliJ, Deixe as seguintes opções marcadas:

![Configurações do IntelliJ](docs/setup-intellij.png)

#### Liferay Plugin & Liferay Workspace

Para instalar o Plugin do Liferay é muito simples, é só ir no canto esquerdo e pesquisar por "Liferay"

![Instalando Plugin do Liferay no Intellij](docs/instalando-plugin-liferay.png)

Após o Plugin do Liferay ser instalado, volte para Projects, e inicie um projeto Liferay com as seguintes configurações:

![Informações Liferay, Nome, Versão e SDK](docs/configuracao-projeto-liferay.png)

Atente-se ao Product Version e ao Project SDK e garanta que eles estejam da seguinte forma:

![Product Version e ao Project SDK](docs/nome-versao-e-sdk-do-projeto-liferay.png)

A IDE levará um tempo para baixar e indexar todos os arquivos do projeto, quando este processo finalizar vá até à aba Gradle > Tasks > Bundle e dê dois cliques em InitBundle. A IDE irá baixa todo o Bundles necessário para o Portal funcionar.

![Iniciando a tarefa initBundle](docs/tarefa-initBundle.png)

Agora está na hora de criarmos nosso Servidor (Local) Liferay. No canto superior direito, clique na barra "Select Run/Debug Configuration" e selecione "Edit Configurations".
Nesta nova janela iremos clicar no símbolo de adição (+) e selecionaremos "Liferay Server" agora só nos resta nomear o servidor e pressionar ok.

![Adicionando o Servidor Local Liferay](docs/servidor-liferay.png)

Com o servidor configurado, é só pressionar Shift + F10 ou clicar no botão de play, ao lado da barra "Select Run/Debug Configuration".

#### JPM

O JPM é um gerenciador de pacotes para Java

Para baixar o JPM é bem simples, basta você abrir o Git Bash e executar os seguintes comandos:

```shell
# Baixa o JPM
curl https://repo1.maven.org/maven2/biz/aQute/bnd/biz.aQute.jpm.run/3.5.0/biz.aQute.jpm.run-3.5.0.jar >t.jar

#Instala o JPM
java -jar t.jar init
```

Com o JPM instalado, vá até Variáveis de Ambiente, na seção Variáveis do Sistema, adicione um novo parâmetro na variável `Path` com o valor `C:\JPM4J\bin`

![Variável de Ambiente para o JPM](docs/variavel-de-ambiente-jpm.png)

```shell
# Para assegurar que o JPM foi instalado corretamente, execute o seguinte comando:
$ jpm version
```

#### Blade

Blade é a maneira mais fácil para os desenvolvedores do Liferay criarem módulos e também é a ferramenta que o Plugin do Liferay usa para criar os módulos como theme, fragments, portlets etc.

Com o JPM instalado, vá até `C:\Users\[nome do usuário]\.liferay-intellij-plugin` assim que identificar o arquivo blade-latest.jar, abra um git bash no diretório atual e execute o seguinte comando:

```shell
# Baixa e Instala o Blade CLI
jpm install blade-latest.jar

# Para assegurar que o Blade CLI foi instalado corretamente, execute o seguinte comando:
$ blade version
```

#### PostgreSQL

O [PostgreSQL](https://www.postgresql.org/) é um Gerenciador de Banco de Dados Objeto Relacional (SGBD) de código aberto.

Para instala-lo vá até o site official do [PostgreSQL](https://www.postgresql.org/), e baixe a versão *11* do PostgreSQL.

Desmarque a opção Stack Builder

![Desabilitando a instalação do Stack Builder](docs/componentes-de-instalacao-do-postgresql.png)

> *Aviso:* Após este passo o instalador irá pedir para que você defina uma senha, gostaria de salientar que não perca esta senha, pois ela não será fácil de redefinir.

_Obs.: Não será necessário alterar a porta, então deixa-a padrão._

Ao finalizar a instalação abra o pgAdmin 4 (será necessário informar a senha recém-criada), na lateral esquerda em Server > PostgreSQL 11 clique com o botão direito em Databases e vá em Create > Database e defina um nome para o seu database. Salve e feche o pgAdmin 4.

## Configurando o Portal Liferay

Agora que todas as ferramentas estão instaladas, está na hora de configurarmos o nosso Portal. No navegador acesse _http://localhost:8080/_ após definir todas as informações do Portal e do Administrador, vá no link "Change" e elecione PostgreSQL em Database Type, feito isso substitua a palavra "lportal" pelo nome do seu banco de dados.

Insira as credenciais do seu banco de dados em User Name e Password e clique em Finish Configuration.

Feito isso é só reiniciar o servidor Liferay que o portal estará pronto para uso.

> Obs.: Caso a senha cadastrada no Portal não funcione, tente acessa usando "test" como senha.
